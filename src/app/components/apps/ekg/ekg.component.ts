import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from '@angular/router';
import {EkgService} from './ekg-service';
import { Message, MenuItem, MessageService } from 'primeng/api';
import { A, C } from '@fullcalendar/core/internal-common';
@Component({
  selector: 'app-ekg',
  templateUrl: './ekg.component.html',
  styleUrls: ['./ekg.component.scss'],
  providers: [MessageService]
})
export class EkgComponent implements OnInit{
  color: string = 'bluegray';

  size: string = 'M';

  liked: boolean = false;

  images: string[] = [];

  selectedImageIndex: number = 0;

  quantity: number = 1;
  queryParamsData: any;
  patientList: any;
  an:any ;
  hn: any;
  title:any;
  fname:any;
  lname:any;
  gender:any;
  age:any;
  phone:any;
  isCardVisible: boolean = true;
  speedDialitems: MenuItem[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    // private renderer: Renderer2,
    // private messageService:MessageService,
    private ekgService:EkgService,
    private messageService: MessageService
    

  )
  {
    let jsonString: any =
    this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
  }

  async ngOnInit() {
    this.isCardVisible = true;
    await this.getPatient();
    this.buttonSpeedDial();
   
  }
  //ข้อมูลคนป่วย
  async getPatient() {
    // console.log('getPatient');
    try {
        const response = await this.ekgService.getPatientInfo(
            this.queryParamsData
        );

        const data: any = response.data;

        this.patientList = await data.data;
        //console.log(this.patientList);

        this.hn = this.patientList.hn;
        this.an = this.patientList.an;
        this.title = this.patientList.patient.title;
        this.fname = this.patientList.patient.fname;
        this.lname = this.patientList.patient.lname;
        this.gender = this.patientList.patient.gender;
        this.age = this.patientList.patient.age;
        this.phone = this.patientList.phone;
        
        console.log("an : ",this.an);
        if(this.an != null || this.an != undefined) {
          this.getEkgdata();
        }

       

    } catch (error: any) {
        console.log(error);
        // this.message.error('getPatient()' +`${error.code} - ${error.message}`);
        // this.messageService.add({
        //     severity: 'dager',
        //     summary: 'เกิดข้อผิดพลาด #',
        //     detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
        // });
    }
}
  //ekg
  async getEkgdata() {
   
    this.spinner.show();
    try {
      console.log("this.an : ",this.an);
        const response = await this.ekgService.getEkg(this.an); 
        const data = await response.data;
        // this.images = data;
        for (let item of data) {
          // ดึงค่า image_url จากแต่ละอ็อบเจ็กต์และเพิ่มเข้าใน images
          this.images.push(item.image_url);
      }
        // console.log(this.images);

        this.spinner.hide(); // แก้ไข this.hideSpinner() เป็น this.spinner.hide()
        console.log(this.images.length);
        if (this.images.length === 0) {
          this.isCardVisible = false;
          this.messageService.add({
              severity: 'warn',
              summary: 'ไม่มีรูปภาพ EKG',
              detail: 'ภาพ EKG ไม่มีในระบบไม่สามารถดูรูปภาพครับ',
          });
        
      }
    } catch (error: any) {
        // console.log(error);
        this.spinner.hide(); // แก้ไข this.hideSpinner() เป็น this.spinner.hide()
    }
}
back() {
  this.router.navigate(['/list-patient']);
}

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
buttonSpeedDial(){
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          this.navigateDoctorOrder()
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          this.navigatePatientInfo()
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-comment-medical',
        command: () => {
          this.navigateAdmisstionNote()
        },
        tooltipOptions: {
          tooltipLabel: 'Admission Note',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          this.navigateConsult()
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          this.navigateLab()
        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'bottom'
        },
      }
    ];
  }   
  
  navigatePatientInfo(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

    }
  
  navigateDoctorOrder(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log('admit id:',jsonString);   
    this.router.navigate(['/doctor-order'], { queryParams: { data: jsonString } });

  }

  navigateAdmisstionNote(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });

  }

  navigateConsult(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/consult'], { queryParams: { data: jsonString } });

  }
  navigateLab(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/lab'], { queryParams: { data: jsonString } });

  }

}
