import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import {SharedModule} from '../../../shared/sharedModule';

import { PatientInfoRoutingModule } from './patient-info-routing.module';
import { PatientInfoComponent } from './patient-info.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { AvatarModule } from 'primeng/avatar';
import { SpeedDialModule } from 'primeng/speeddial';

@NgModule({
  declarations: [
    PatientInfoComponent,
    
  ],
  imports: [
    CommonModule,SharedModule,
    PatientInfoRoutingModule,
    NgxSpinnerModule,
    ButtonModule,AvatarModule,
    SpeedDialModule
  ]
})
export class PatientInfoModule { }
